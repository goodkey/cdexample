package at.badkey.cdexample;

public interface Playable {
	public void play();
	public void stop();
	public void pause();
}

