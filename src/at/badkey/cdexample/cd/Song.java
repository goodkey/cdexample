package at.badkey.cdexample.cd;

import java.util.ArrayList;

import at.badkey.cdexample.AbstractPlayable;

public class Song extends AbstractPlayable{
	
	private ArrayList<CD> cds = new ArrayList<CD>();
	
	public Song(String title, int length) {
		super(title, length);
		// TODO Auto-generated constructor stub
	}
	
	public void addCD(CD cd) {
		cds.add(cd);
	}
	
	public ArrayList<CD> getAllCDsWithSong(){
		return cds;
	}

	@Override
	public void play() {
		System.out.println("Played song "+getTitle());
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped song");
		
	}

	@Override
	public void pause() {
		System.out.println("Paused song");
		
	}

}
