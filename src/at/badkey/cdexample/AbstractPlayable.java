package at.badkey.cdexample;

public abstract class AbstractPlayable implements Playable{
	private String title;
	private int length;
	
	public AbstractPlayable(String title, int length) {
		this.title = title;
		this.length = length;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getLength(){
		return this.length;
	}
	
	
}
