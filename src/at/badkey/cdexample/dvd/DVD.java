package at.badkey.cdexample.dvd;

import java.util.ArrayList;

public class DVD {

	private String name;
	private ArrayList<Title> titles = new ArrayList<Title>();
	
	public DVD(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void addTitle(Title s) {
		titles.add(s);
	}
	
	public ArrayList<Title> getTitles() {
		return titles;
	}
	
}
