package at.badkey.cdexample.dvd;

import java.util.ArrayList;

import at.badkey.cdexample.AbstractPlayable;

public class Title extends AbstractPlayable{
	
	private ArrayList<DVD> dvds = new ArrayList<DVD>();

	
	public Title(String title, int length) {
		super(title, length);
	}
	
	public void addDVD(DVD cd) {
		dvds.add(cd);
	}
	
	public ArrayList<DVD> getAllDVDsWithSong(){
		return dvds;
	}

	@Override
	public void play() {
		System.out.println("Played dvd "+getTitle());
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped dvd");
		
	}

	@Override
	public void pause() {
		System.out.println("Paused dvd");
		
	}
	

}
