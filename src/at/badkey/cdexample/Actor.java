package at.badkey.cdexample;

import java.util.ArrayList;

import at.badkey.cdexample.cd.CD;
import at.badkey.cdexample.dvd.DVD;

public class Actor {
	
	private String firstName;
	private String lastName;
	private ArrayList<CD> cds = new ArrayList<CD>();
	private ArrayList<DVD> dvds = new ArrayList<DVD>();
	
	public Actor(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public ArrayList<DVD> getAllDVDs() {
		return dvds;
	}
	
	public ArrayList<CD> getAllRecords() {
		return cds;
	}
	
	public void addCD(CD cd) {
		cds.add(cd);
	}
	
	public void addDVD(DVD dvd) {
		dvds.add(dvd);
	}

}
